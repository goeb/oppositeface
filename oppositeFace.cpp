/*********************************************************************************************

Copyright 2009, 2014 Stefan Goebel - <oppositeFace -at- subtype -dot- de>

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU Lesser General Public License as published by the Free Software Foundation, either version
3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with this
program. If not, see <http://www.gnu.org/licenses/>.

**********************************************************************************************

oppositeFace 0.2 - Autodesk Maya plugin to find a polygon face opposing the selected face.

Usage:  * Make sure the plugin is loaded in the plugin manager.
        * Select a face of a polygon mesh.
        * Run the 'oppositeFace' MEL command.
        * The return value of the command is a list with the following information:
          [ face_number, distance, hit_point_x, hit_point_y, hit_point_z ]
        * All these values are strings, so you need to explicitely convert them if required!
        * The face_number specifies the opposing face, the distance is the distance of the
          center point of the selected face to the opposing face in negative polygon normal
          direction, the hit_point coordinates are the coordinates where the line starting
          from the center of the selected face in negative normal direction intersects the
          opposing face, these are world space coordinates.
        * If no opposing face is found, the face_number will be "-1", all other return values
          will be "0".
        * The search for the face will actually start at a point a bit away from the face's
          center point (default value is a distance of 0.01) to avoid hitting the selected
          face itself. You can change the start distance with the -o parameter.
        * The tolerance (see MFnMesh::closestIntersection in the API guide) can be changed
          with the -t parameter (default value is 1e-6).
        * The maximum search distance can be changed with the -d parameter, default value is
          1000.
        * All these values must be specified in cm.

Todo:   * Add possibility to query only specific information (eg. only the face number).
        * Add caching of the results, to query additional information without recalculation.

*********************************************************************************************/

#include <maya/MArgDatabase.h>
#include <maya/MArgList.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MDagPath.h>
#include <maya/MGlobal.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItSelectionList.h>
#include <maya/MFnMesh.h>
#include <maya/MFnPlugin.h>
#include <maya/MPxCommand.h>
#include <maya/MSelectionList.h>
#include <maya/MString.h>
#include <maya/MSyntax.h>

#define VERSION                 "0.2"
#define API                     "Any"

#define offsetFlag              "-o"
#define offsetFlagLong          "-offset"
#define toleranceFlag           "-t"
#define toleranceFlagLong       "-tolerance"
#define distanceFlag            "-d"
#define distanceFlagLong        "-distance"

class oppositeFace: public MPxCommand {

        public:
                MStatus         doIt (const MArgList&);
                static void*    creator ();
                bool            isUndoable () const;
                static MSyntax  newSyntax ();

};

MStatus oppositeFace::doIt (const MArgList& args) {

        MStatus stat;

        double offset      = 0.01;
        double tolerance   = 1e-6;
        double maxDistance = 1000;

        MArgDatabase argData (syntax (), args, &stat);
        if (stat != MS::kSuccess) {
                displayError ("One or more invalid arguments!");
                return stat;
        }

        if (argData.isFlagSet (offsetFlag)) {
                argData.getFlagArgument (offsetFlag, 0, offset);
        }

        if (argData.isFlagSet (toleranceFlag)) {
                argData.getFlagArgument (toleranceFlag, 0, tolerance);
        }

        if (argData.isFlagSet (distanceFlag)) {
                argData.getFlagArgument (distanceFlag, 0, maxDistance);
        }

        MSelectionList selList;
        stat = MGlobal::getActiveSelectionList (selList);
        if (stat != MS::kSuccess) {
                displayError ("Error getting selection list: " + stat.errorString ());
                return stat;
        }

        MItSelectionList itSelList (selList, MFn::kMeshPolygonComponent, &stat);
        if (stat != MS::kSuccess) {
                displayError ("Error iterating selection list: " + stat.errorString ());
                return stat;
        }

        MObject obj;
        MDagPath dagPath;
        stat = itSelList.getDagPath (dagPath, obj);
        if (stat != MS::kSuccess || obj.isNull ()) {
                displayError ("No faces selected!");
                return MS::kFailure;
        }

        MFnMesh mesh (dagPath, &stat);
        if (stat != MS::kSuccess) {
                displayError ("Could not get mesh: " + stat.errorString ());
                return stat;
        }

        MItMeshPolygon itMeshPoly (dagPath, obj, &stat);
        if (stat != MS::kSuccess) {
                displayError ("Error iterating polygons: " + stat.errorString ());
                return stat;
        }

        if (selList.length () != 1 || itMeshPoly.count () != 1) {
                displayError ("Please select one (and only one) face!");
                return MS::kFailure;
        }

        MPoint center = itMeshPoly.center (MSpace::kWorld, &stat);
        if (stat != MS::kSuccess) {
                displayError ("Error getting center of face: " + stat.errorString ());
                return stat;
        }

        MVector normal;
        stat = itMeshPoly.getNormal (normal, MSpace::kWorld);
        if (stat != MS::kSuccess) {
                displayError ("Could not get face normal: " + stat.errorString ());
                return stat;
        }

        MFloatPoint  fCenter (center);
        MFloatVector fNormal (normal * -1);

        MFloatPoint  hitPoint;
        float        distance;
        int          face;

        fNormal.normalize ();
        fCenter += fNormal * offset;

        bool hit = mesh.anyIntersection (
                fCenter,
                fNormal,
                NULL,
                NULL,
                false,
                MSpace::kWorld,
                maxDistance,
                false,
                NULL,
                hitPoint,
                &distance,
                &face,
                NULL,
                NULL,
                NULL,
                tolerance,
                &stat
        );

        if (stat != MS::kSuccess) {
                displayError ("Error getting intersection: " + stat.errorString ());
                return stat;
        }

        if (hit) {
                distance += offset;
                MString result;
                result += face;       appendToResult (result); result.clear ();
                result += distance;   appendToResult (result); result.clear ();
                result += hitPoint.x; appendToResult (result); result.clear ();
                result += hitPoint.y; appendToResult (result); result.clear ();
                result += hitPoint.z; appendToResult (result); result.clear ();
        }
        else {
                appendToResult ("-1");
                for (int i = 0; i < 5; i++) appendToResult ("0");
        }

        return MS::kSuccess;

}

void* oppositeFace::creator () {
        return new oppositeFace;
}

bool oppositeFace::isUndoable () const {
        return false;
}

MSyntax oppositeFace::newSyntax () {
        MSyntax syntax;
        syntax.addFlag (offsetFlag,    offsetFlagLong,    MSyntax::kDouble);
        syntax.addFlag (toleranceFlag, toleranceFlagLong, MSyntax::kDouble);
        syntax.addFlag (distanceFlag,  distanceFlagLong,  MSyntax::kDouble);
        return syntax;
}

MStatus initializePlugin (MObject obj) {
        MStatus stat;
        MFnPlugin plugin (obj, "Stefan Goebel", VERSION, API, &stat);
        if (stat == MS::kSuccess) {
                stat = plugin.setName ("oppositeFace", true);
                if (stat == MS::kSuccess) {
                        stat = plugin.registerCommand (
                                "oppositeFace",
                                oppositeFace::creator,
                                oppositeFace::newSyntax
                        );
                }
        }
        return stat;
}

MStatus uninitializePlugin (MObject obj) {
        MFnPlugin plugin (obj);
        return plugin.deregisterCommand ("oppositeFace");
}

// :indentSize=8:tabSize=8:noTabs=true:mode=c++:maxLineLen=94: