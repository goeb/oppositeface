CPP        = g++-4.1

CPPFLAGS1  = -c -m64 -O3 -pthread -pipe
CPPFLAGS1 += -I/usr/autodesk/maya2009-x64/include -I/usr/X11R6/include
CPPFLAGS1 += -D_BOOL -DLINUX_64 -DREQUIRE_IOSTREAM
CPPFLAGS1 += -fPIC -Wno-deprecated -fno-gnu-keywords

CPPFLAGS2  = -shared -m64 -O3 -pthread -pipe -Bsymbolic
CPPFLAGS2 += -D_BOOL -DLINUX -DREQUIRE_IOSTREAM
CPPFLAGS2 += -Wno-deprecated -fno-gnu-keywords -Wl
CPPFLAGS2 += -L/usr/autodesk/maya2009-x64/lib -lOpenMaya -lFoundation

all: oppositeFace

oppositeFace.o: oppositeFace.cpp
	$(CPP) $(CPPFLAGS1) oppositeFace.cpp

oppositeFace: oppositeFace.o
	$(CPP) $(CPPFLAGS2) -o oppositeFace.so oppositeFace.o

clean:
	rm -f *.o *.so
